﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoalSheetpartPlanning
{


    public partial class Test : Form
    {
        DataTable dt_candidateResult;
        public Test()
        {
            InitializeComponent();
        }

        private void Test_Load(object sender, EventArgs e)
        {

          


            string json = string.Empty;
            string qry = "select Candidate_Email,[competencies_array],user_assessment_id from [tbl_AssessmentTest_Data] where [syncUpdated] IS NULL ";
            DataTable dt = DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qry));

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string candidate_email = Convert.ToString(dr["Candidate_Email"]);
                    string candidate_user_assessment_id = Convert.ToString(dr["user_assessment_id"]);
                    //Getting Position and Position type
                    DataTable dt_candidate_details = fnGetCandidateDetails(candidate_email);

                    if (dt_candidate_details.Rows.Count > 0)
                    {
                        json = "[ " + Convert.ToString(dr["competencies_array"]) + " ] ";

                        string position = dt_candidate_details.Rows[0]["position"].ToString();
                        string exam_type = dt_candidate_details.Rows[0]["exam_type"].ToString();
                        DataTable tester = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));
                        DataTable competencies_result = GenerateTransposedTable(tester);


                        string finalResult = fnGetFinalResult(competencies_result, position, exam_type, out dt_candidateResult);


                        foreach (DataRow item in dt_candidateResult.Rows)
                        {
                            fnInsertCandidateResult(candidate_email, candidate_user_assessment_id,Convert.ToString(item["Skills"]), Convert.ToString(item["ExpectedResult"]), Convert.ToString(item["ExpectedResult_score"]), Convert.ToString(item["ActualResult"]), Convert.ToString(item["ActualResult_score"]), Convert.ToString(item["isAccepted"]), Convert.ToString(item["isMandatory"]));

                           
                        }

                        dt_candidateResult.Rows.Clear();
                        //check if person fits for any other position
                        if (finalResult == CommonMethods.rejected)
                        {


                            string qr = "select position_Name  from tbl_Position_Master";
                            DataTable dt_postion = DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qr));


                            ArrayList al_candidateFitFor = new ArrayList();

                            foreach (DataRow item in dt_postion.Rows)
                            {
                                string candidateResult = fnGetFinalResult(competencies_result, Convert.ToString(item["position_Name"]) , exam_type, out dt_candidateResult);

                                if (candidateResult == CommonMethods.accepted)
                                {
                                    al_candidateFitFor.Add(Convert.ToString(item["position_Name"]));
                                }


                            }

                          


                            var strings = from object o in al_candidateFitFor
                                          select o.ToString();

                            var suggestedPosition = string.Join(";", strings.ToArray());
                            fnInsertSuggestedPosition(candidate_email, candidate_user_assessment_id, position, finalResult, suggestedPosition.ToString().TrimEnd(';'));

                        }



                    }
                }
            }











          
            

        }

        private void fnInsertSuggestedPosition(string email,string assesment_id,string applied_position,string applied_position_result,string suggested_position)
        {
            try
            {
                string qry = "INSERT INTO [tbl__Reject_Candidates_Suggested_position] ([candidate_email],[User_Assessment_Id],[applied_position],[actual_result],[system_position_suggestion] ,[inserted_at]) VALUES  ('"+ email + "','" + assesment_id + "','" + applied_position + "','" + applied_position_result + "','" + suggested_position + "','" + DateTime.Now.ToString("dd MMM yyyy HH:mm") + "')";
                int i = DBUtils.ExecuteSQLCommand(new System.Data.SqlClient.SqlCommand(qry));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void fnInsertCandidateResult(string email, string user_assesment_id, string skill, string expected_result, string expected_result_score, string actual_result, string actual_result_score, string is_accepted, string is_isMandatory)
        {
            try
            {
                string qry = "INSERT INTO [tbl_System_Competencies_Result] ([Candidate_Email],[User_Assessment_Id],[Skill] ,[ExpectedResult],[ExpectedResult_score],[ActualResult] ,[ActualResult_score],[isAccepted],[isMandatory],inserted_at) VALUES ('" + email+ "','" + user_assesment_id + "','" + skill + "','" + expected_result + "','" + expected_result_score + "','" + actual_result + "','" + actual_result_score + "','" + is_accepted + "','" + is_isMandatory + "','"+DateTime.Now.ToString("dd MMM yyyy HH:mm")+"')";
                int i = DBUtils.ExecuteSQLCommand(new System.Data.SqlClient.SqlCommand(qry));

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private DataTable fnGetCandidateDetails(string candidate_email)
        {
            try
            {
                string qry = "SELECT TOP 1 (select UPPER([position_Name]) from [tbl_Position_Master] where [position_Id]=MT.[position_Id]) as position,UPPER(position_type) as exam_type   FROM [tbl_JombayExam_NewInterviewMaster] as MT where MT.User_Email='" + candidate_email + "'";
                return DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qry));

                  

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        internal Dictionary<string, string> GetDict(DataTable dt)
        {
            return dt.AsEnumerable()
              .ToDictionary<DataRow, string, string>(row => row.Field<string>(0),
                                        row => row.Field<string>(1));
        }

        private string fnGetFinalResult(DataTable competencies_result, string position,string exam_type, out DataTable dt_candidateResult)
        {
            Boolean isSkillMatrixPresent= fnCheck_SkillMatrixPresent(position, exam_type);

            if (isSkillMatrixPresent)
            {

                Boolean isRejected = false;


                DataTable dt_final = new DataTable();
                dt_final.Clear();
                dt_final.Columns.Add("Skills");
                dt_final.Columns.Add("ExpectedResult");
                dt_final.Columns.Add("ExpectedResult_score");
                dt_final.Columns.Add("ActualResult");
                dt_final.Columns.Add("ActualResult_score");
                dt_final.Columns.Add("isAccepted");

                dt_final.Columns.Add("isMandatory");



                foreach (DataRow dr in competencies_result.Rows)
                {
                    string skill = Convert.ToString(dr["Skill"]);



                    //get detals for position and type 
                    DataTable dt_tempDetails = fnGetSkillDataFromDb(skill, position,exam_type);

                    string expectedResult = string.Empty;
                    Boolean isMandatory = false;

                    if (dt_tempDetails.Rows.Count > 0)
                    {
                        expectedResult = Convert.ToString(dt_tempDetails.Rows[0]["level_name"]);
                        isMandatory = Convert.ToBoolean(dt_tempDetails.Rows[0]["isMandatory"]);
                    }

                    string actualResult = Convert.ToString(dr["Result"]);

                    string isAccepted = string.Empty;


                    string exp_res_priority = fnGetResultPriority(expectedResult.ToUpper());
                    string act_Res_priority = fnGetResultPriority(actualResult.ToUpper());




                    if (exp_res_priority == "0")
                    {
                        isAccepted = "";
                    }

                    else if (Convert.ToInt32(act_Res_priority) >= Convert.ToInt32(exp_res_priority))
                    {
                        isAccepted = CommonMethods.accepted;
                    }
                    else
                    {
                        isAccepted = CommonMethods.rejected;
                        if (isMandatory)
                        {
                            isRejected = true;

                        }
                    }




                    dt_final.Rows.Add(skill, expectedResult, exp_res_priority, actualResult, act_Res_priority, isAccepted,Convert.ToString(isMandatory));



                }
                dt_candidateResult = dt_final;

                if (isRejected)
                {
                    return CommonMethods.rejected;
                }
                else
                {
                    return CommonMethods.accepted;
                }


              
            }
            else {
                dt_candidateResult = null;
                return "Incomplete Skill Matrix";
            }
           

        }

        private bool fnCheck_SkillMatrixPresent(string position,string type)
        {
            try
            {
                string qry = "select count(*) from tbl_Result_Decision_Matrix where UPPER(position_name)='" + position + "' and UPPER( exam_type)='" + type + "'";

                string res = DBUtils.SqlSelectScalar(new System.Data.SqlClient.SqlCommand(qry));
                if (Convert.ToInt32(res) == 0)
                {
                    return false;
                }
                else {
                    return true;
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private DataTable fnGetSkillDataFromDb(string skill, string position,string exam_type)
        {
            try
            {
                string qry = "select skill_name,level_name, isMandatory from [tbl_Result_Decision_Matrix] where [position_Name]='" + position + "' and exam_type='"+ exam_type + "' and skill_name='" + skill + "'";
                return DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qry));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private string fnGetResultPriority(string result)
        {
            try
            {
                string qry = "select level_priority FROM [tbl_Level_Master] where level_name = '" + result + "'";

                string result1 = DBUtils.SqlSelectScalar(new System.Data.SqlClient.SqlCommand(qry));


                if (string.IsNullOrEmpty(result1)) {
                    return "0";
                }
                else {
                    return result1;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();

            // Add columns by looping rows

            // Header row's first column is same as in inputTable
            //outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

            // Header row's second column onwards, 'inputTable's first column taken
            //foreach (DataRow inRow in inputTable.Rows)
            //{
            //    string newColName = inRow[0].ToString();
            //    outputTable.Columns.Add(newColName);
            //}

          
                outputTable.Columns.Add("Skill");
                outputTable.Columns.Add("Result");
          


            // Add rows by looping columns        
            for (int rCount = 0; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }
    }
}
