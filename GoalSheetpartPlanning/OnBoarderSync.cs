﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoalSheetpartPlanning
{
    public partial class OnBoarderSync : Form
    {
        public string HR = "HR";
        public string CORE_TEAM = "CORE TEAM";

        public string ACCEPTED = "Accepted";
        public string REJECTED = "Rejected";

        public string fname = string.Empty;


        public OnBoarderSync()
        {
            InitializeComponent();
        }

        private void OnBoarderSync_Load(object sender, EventArgs e)
        {
            try
            {



                

                var watch = System.Diagnostics.Stopwatch.StartNew();
                // the code that you want to measure comes here
              
                fname = DateTime.Now.ToString("dd-MMM-yyyy") + ".txt";

                labelStatus.Text = "Action Started...";
                Application.DoEvents();
                string qry = "select Candidate_Email,[AggregateScore] from [tbl_AssessmentTest_Data] where [syncUpdated] IS NULL ";

                DataTable dt = DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qry));

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {

                        labelStatus.Text = "Candidate Details Fetched...";
                        Application.DoEvents();

                        string candidate_email = Convert.ToString(dr["Candidate_Email"]);
                        string aggregateScore = Convert.ToString(dr["AggregateScore"]);

                        string result = fnGetResultAsPerScore(aggregateScore);


                        string message = "1 . Details Fetched For : " + candidate_email + " - " + aggregateScore + " - " + result;
                        fnCreateLog(message, fname);

                        if (string.IsNullOrEmpty(result))
                        {
                            fnCreateLog("Please check the aggregate score for :" + aggregateScore, fname);
                        }
                        else
                        {
                            fnUpdateCandidateRes(result, candidate_email);
                        }


                    }

                    watch.Stop();
                    var elapsedMs = watch.ElapsedMilliseconds;
                    labelStatus.Text = "Action complete in (milisec) ..." + Convert.ToString(elapsedMs);
                    Application.Exit();
                }
                else {
                    watch.Stop();
                    var elapsedMs = watch.ElapsedMilliseconds;


                    fnCheckDataSyncOr_Not();

                    fnCreateLog("No data found to sync at  :  " + DateTime.Now.ToString("dd/MMM/yyyy HH:MM"), fname);

                    labelStatus.Text = "No data found to sync,Action Complete in (milisec) : " + Convert.ToString(elapsedMs);
                    Application.Exit();

                }

                
            }
            catch (Exception ex)
            {
                fnCreateLog(ex.Message.ToString(), fname);

                //MessageBox.Show(ex.Message.ToString());
            }
          
        }

        private void fnCheckDataSyncOr_Not()
        {
            try
            {
                //Check > candidate completed exam
                string qry1 = "select * ,(select top 1 tool_status from [tbl_Incompleted_AssessmentTest_Data] where [Candidate_Email] =  tbl_JombayExam_NewInterviewMaster.[User_Email]) as tool_status from tbl_JombayExam_NewInterviewMaster where ( [User_Email] In ( SELECT [Candidate_Email] from  [tbl_Incompleted_AssessmentTest_Data]) And [User_Email] NOT IN ( SELECT [Candidate_Email] from  tbl_AssessmentTest_Data))  and (DATEDIFF(HOUR,created_At,GETDATE()) >= " + CommonMethods.fnFromTime() + " and   DATEDIFF(HOUR,created_At,GETDATE()) <= " + CommonMethods.fnToTime() + ")";

                DataTable dt1 = DBUtils.SQLSelect(new SqlCommand(qry1));

                if (dt1.Rows.Count > 0) {

                    foreach (DataRow item in dt1.Rows)
                    {
                        fnSend_Email_ExamIncompleted(Convert.ToString(item["User_Email"]), Convert.ToString(item["User_Name"]), Convert.ToDateTime(item["created_At"]).ToString("dd/MMM/yyy HH:MM"), Convert.ToString(item["tool_status"]));

                    }
                  
                }
                else
                {
                    //If not then issue with sync from jombay


                    string qry = "select * ,DATEDIFF(HOUR,created_At,GETDATE()) from tbl_JombayExam_NewInterviewMaster where User_Email NOT IN (select Candidate_Email from tbl_AssessmentTest_Data where syncUpdated = 'True')  and (DATEDIFF(HOUR,created_At,GETDATE()) >= "+CommonMethods.fnFromTime()+ " and   DATEDIFF(HOUR,created_At,GETDATE()) <= " + CommonMethods.fnToTime() + ")";


                    DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            fnSend_Email_DataNotReceived(Convert.ToString(item["User_Email"]), Convert.ToString(item["User_Name"]), Convert.ToDateTime(item["created_At"]).ToString("dd/MMM/yyy HH:MM"));
                        }
                    }
                }
                

            }
            catch (Exception ex)
            {
                fnCreateLog(ex.Message.ToString(), fname);
                throw ex;
            }
        }

        private void fnSend_Email_ExamIncompleted(string userEmail, string userName, string createdAt,string examStatus)
        {
            try
            {
                try
                {
                    string emailId = string.Empty;
                    string password = string.Empty;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Timeout = 2 * 300000;
                    string qry = "SELECT * FROM tblSMTPSettingsMaster";
                    DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));
                    foreach (DataRow dr in dt.Rows)
                    {
                        smtp.Host = dr["Servername"].ToString();
                        smtp.Port = Convert.ToInt16(dr["Portno"].ToString());
                        smtp.EnableSsl = Convert.ToBoolean(dr["EnableSsl"].ToString());
                        emailId = dr["Emailid"].ToString();
                        password = dr["Usrpassword"].ToString();
                    }
                    smtp.Credentials = new System.Net.NetworkCredential(emailId, password);


                    System.Net.Mail.MailMessage mail1 = new System.Net.Mail.MailMessage();

                    mail1.From = new MailAddress(emailId, "Do Not Reply");
                    mail1.Subject = "Exam Incomplete!";

                    string qryToEmail = "select [hr_Email] FROM [tbl_User_Access] where Role = '" + HR + "' ";
                    DataTable dt_To = DBUtils.SQLSelect(new SqlCommand(qryToEmail));
                    foreach (DataRow dr in dt_To.Rows)
                    {
                        mail1.Bcc.Add(Convert.ToString(dr["hr_Email"]));
                    }





                    mail1.IsBodyHtml = true;
                    string bodyUser = fnFetchEMailBody_ExamIncompleted(userEmail, userName, createdAt,examStatus);
                    mail1.Body = bodyUser;
                    smtp.Send(mail1);

                    labelStatus.Text = "Mail Sent to HR-Exam Incomplete...";
                    Application.DoEvents();

                    fnCreateLog("3 . Mail send to HR For-Exam Incomplete:" + userEmail, fname);



                }
                catch (Exception ex)
                {
                    fnCreateLog(ex.Message.ToString() + " - " + userEmail, fname);

                    //throw ex;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private string fnFetchEMailBody_ExamIncompleted(string userEmail, string userName, string createdAt, string examStatus)
        {
            try
            {
                string body;


                string binaryPath =
         System.IO.Path.GetDirectoryName(
             System.Reflection.Assembly.GetEntryAssembly().Location) + "\\ExamIncomplete.html";

                //To read html file
                StreamReader readTemplateFile = null;



                readTemplateFile = new StreamReader(binaryPath);

                string allContents = readTemplateFile.ReadToEnd();


                allContents = allContents.Replace("@#User_Name#@", userName);
                allContents = allContents.Replace("@#User_Email#@", userEmail);
                allContents = allContents.Replace("@#Created_At#@", createdAt);
                

                string json = "[ " + examStatus + " ] ";

                if (!string.IsNullOrEmpty(examStatus))


                {
                    DataTable tester = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                    DataTable dtClone = tester.Clone(); //just copy structure, no data
                    for (int i = 0; i < dtClone.Columns.Count; i++)
                    {
                        if (dtClone.Columns[i].DataType != typeof(string))
                            dtClone.Columns[i].DataType = typeof(string);
                    }

                    foreach (DataRow dr in tester.Rows)
                    {
                        dtClone.ImportRow(dr);
                    }

                    DataTable transposedData = GenerateTransposedTable(dtClone, "Exam");

                    DataTable dt_New = new DataTable();
                    dt_New.Clear();
                    dt_New.Columns.Add("Test");
                    dt_New.Columns.Add("Result");
                    foreach (DataRow dr in transposedData.Rows)
                    {
                        string res = string.Empty;
                        String testName = Convert.ToString(dr["Test"]);


                       
                            testName = Convert.ToString(dr["Test"]).Replace("_", " ");
                        testName = testName.Replace("is", " ");



                        if (Convert.ToString(dr["Data"]).Equals("True"))
                        {
                            res = "Completed";
                        }
                        if (Convert.ToString(dr["Data"]).Equals("False"))
                        {
                            res = "Pending";
                        }

                        dt_New.Rows.Add(testName, res);
                    }


                    StringBuilder sb_examStatus = new StringBuilder();

                


                    foreach (DataRow item in dt_New.Rows)
                    {



                        sb_examStatus.Append(@"<tr> <td>"+ Convert.ToString(item["Test"]) + "</td><td style='font - weight: bold;' >"+ Convert.ToString(item["Result"]) + "</td> </ tr > ");






                    }


                    allContents = allContents.Replace("@#Exam_Details#@", sb_examStatus.ToString());

                }

                    body = allContents;
                return body;
            }
            catch (Exception ex)
            {
                fnCreateLog(ex.Message.ToString(), fname);
                throw ex;
            }
        }

        private void fnSend_Email_DataNotReceived(string email,string name,string createdAt)
        {
            try
            {
                try
                {
                    string emailId = string.Empty;
                    string password = string.Empty;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Timeout = 2 * 300000;
                    string qry = "SELECT * FROM tblSMTPSettingsMaster";
                    DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));
                    foreach (DataRow dr in dt.Rows)
                    {
                        smtp.Host = dr["Servername"].ToString();
                        smtp.Port = Convert.ToInt16(dr["Portno"].ToString());
                        smtp.EnableSsl = Convert.ToBoolean(dr["EnableSsl"].ToString());
                        emailId = dr["Emailid"].ToString();
                        password = dr["Usrpassword"].ToString();
                    }
                    smtp.Credentials = new System.Net.NetworkCredential(emailId, password);


                    System.Net.Mail.MailMessage mail1 = new System.Net.Mail.MailMessage();

                    mail1.From = new MailAddress(emailId, "Do Not Reply");
                    mail1.Subject = "Result not received!";

                    string qryToEmail = "select [hr_Email] FROM [tbl_User_Access] where Role = '" + HR + "' ";
                    DataTable dt_To = DBUtils.SQLSelect(new SqlCommand(qryToEmail));
                    foreach (DataRow dr in dt_To.Rows)
                    {
                        mail1.Bcc.Add(Convert.ToString(dr["hr_Email"]));
                    }


                   string jombayemail_list= CommonMethods.fnGetJombayTeamEmail();
                    string[] jombayEmail = jombayemail_list.Split(';');


                    foreach (string id in jombayEmail)
                    {
                        if (!string.IsNullOrEmpty(id)) {
                            mail1.Bcc.Add(id);
                        }
                    }

                    mail1.IsBodyHtml = true;
                    string bodyUser = fnFetchEMailBody_ResultNotFetched(email,name,createdAt);
                    mail1.Body = bodyUser;
                    smtp.Send(mail1);

                    labelStatus.Text = "Mail Sent to HR- Data not received...";
                    Application.DoEvents();

                    fnCreateLog("3 . Mail send to HR For-Data not received:" + email, fname);



                }
                catch (Exception ex)
                {
                    fnCreateLog(ex.Message.ToString() + " - " + email, fname);

                    //throw ex;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private string fnFetchEMailBody_ResultNotFetched(string email, string name, string createdAt)
        {
            try
            {
                string body;


                string binaryPath =
         System.IO.Path.GetDirectoryName(
             System.Reflection.Assembly.GetEntryAssembly().Location) + "\\ResultNotSync.html";

                //To read html file
                StreamReader readTemplateFile = null;



                readTemplateFile = new StreamReader(binaryPath);

                string allContents = readTemplateFile.ReadToEnd();


                    allContents = allContents.Replace("@#User_Name#@", name);
                allContents = allContents.Replace("@#User_Email#@", email);
                allContents = allContents.Replace("@#Created_At#@", createdAt);
                

                body = allContents;
                return body;
            }
            catch (Exception ex)
            {
                fnCreateLog(ex.Message.ToString(), fname);
                throw ex;
            }
        }

        private void fnUpdateCandidateRes(string res,string userEmail)
        {
            try
            {

                
                string updateStatus_qry = "UPDATE [tbl_JombayExam_NewInterviewMaster] SET [isAccepted] = '"+res+"'  ,statusBy ='SYSTEM',statusAddedAt ='" + DateTime.Now.ToString("dd/MMM/yyy HH:mm") + "' WHERE [User_Email] = '" + userEmail + "'";

                int i = DBUtils.ExecuteSQLCommand(new System.Data.SqlClient.SqlCommand(updateStatus_qry));

                labelStatus.Text = "Candidate Result Updated...";
                Application.DoEvents();

                string message = "Candidate Email : " + userEmail; 

                if (i > 0)
                {

                    fnCreateLog("2 . Result updated at interview Master For :" +  message, fname);
                    fnSendMail(userEmail);
                    fnUpdateSync(userEmail);

                }
                else {

                    fnCreateLog("2 . failed to update result at Interview Master :" +  message , fname);
                    //MessageBox.Show("Failed to update...");
                }

               
            }
            catch (Exception ex)
            {
                fnCreateLog(ex.ToString(), fname);
            }
        }

        private string fnGetResultAsPerScore(string aggregateScore)
        {
           
                string query = "select [Result] from tbl_AggregateResult_Master where [AggregateCode]='" + aggregateScore + "'";

                string res = DBUtils.SqlSelectScalar(new System.Data.SqlClient.SqlCommand(query));

                return res;


          
        }



        private void fnSendMail(string userEmail)
        {
            try
            {
                string emailId = string.Empty;
                string password = string.Empty;
                SmtpClient smtp = new SmtpClient();
                smtp.Timeout = 2 * 300000;
                string qry = "SELECT * FROM tblSMTPSettingsMaster";
                DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));
                foreach (DataRow dr in dt.Rows)
                {
                    smtp.Host = dr["Servername"].ToString();
                    smtp.Port = Convert.ToInt16(dr["Portno"].ToString());
                    smtp.EnableSsl = Convert.ToBoolean(dr["EnableSsl"].ToString());
                    emailId = dr["Emailid"].ToString();
                    password = dr["Usrpassword"].ToString();
                }
                smtp.Credentials = new System.Net.NetworkCredential(emailId, password);


                System.Net.Mail.MailMessage mail1 = new System.Net.Mail.MailMessage();

                mail1.From = new MailAddress(emailId, "Do Not Reply");
                mail1.Subject = "Result updated By system!";

                string qryToEmail = "select [hr_Email] FROM [tbl_User_Access] ";
                DataTable dt_To = DBUtils.SQLSelect(new SqlCommand(qryToEmail));
                foreach(DataRow dr in dt_To.Rows)
                {
                    mail1.Bcc.Add(Convert.ToString(dr["hr_Email"]));
                }

                //string qryCCEmail = "select [hr_Email] FROM [tbl_User_Access] where Role = '" + CORE_TEAM + "'";
                //DataTable dt_CC = DBUtils.SQLSelect(new SqlCommand(qryCCEmail));
                //foreach (DataRow dr in dt_CC.Rows)
                //{
                //    mail1.CC.Add(Convert.ToString(dr["hr_Email"]));
                //}



                mail1.IsBodyHtml = true;
                string bodyUser = fnFetchEMailBody(userEmail);
                mail1.Body = bodyUser;
                smtp.Send(mail1);

                labelStatus.Text = "Mail Sent to HR...";
                Application.DoEvents();

                fnCreateLog("3 . Mail send to HR and Core Team For:" + userEmail, fname);

               

            }
            catch (Exception ex)
            {
                fnCreateLog(ex.Message.ToString() + " - " + userEmail, fname);

                //throw ex;
            }


        }

        private void fnUpdateSync(string userEmail)
        {
            try
            {
                string qry = "UPDATE [tbl_AssessmentTest_Data] SET [syncUpdated] = 'True' ,[syncUpdatedAt]='" + Convert.ToString(DateTime.Now) + "'  WHERE [Candidate_Email]='" + userEmail + "'";

                int i = DBUtils.ExecuteSQLCommand(new SqlCommand(qry));

                labelStatus.Text = "Test Sync details updated...";
                Application.DoEvents();

                fnCreateLog("4 . tbl_AssessmentTest_Data sync Updated for :" + userEmail, fname);

            }
            catch (Exception ex)
            {
                fnCreateLog(ex.Message.ToString() + " : " + userEmail, fname);
                //MessageBox.Show(ex.ToString());
            }

        }

        protected string fnFetchEMailBody(string userEmail)
        {


        string body, name = "", email = "";


            string binaryPath =
     System.IO.Path.GetDirectoryName(
         System.Reflection.Assembly.GetEntryAssembly().Location) + "\\candidateDetails.html";

            //To read html file
            StreamReader readTemplateFile = null;



        readTemplateFile = new StreamReader(binaryPath);

        string allContents = readTemplateFile.ReadToEnd();


            string query = "SELECT TOP 1 tbl_JombayExam_NewInterviewMaster.Id_Exam,tbl_JombayExam_NewInterviewMaster.User_Email,tbl_JombayExam_NewInterviewMaster.User_Name,tbl_JombayExam_NewInterviewMaster.age,tbl_JombayExam_NewInterviewMaster.remark,tbl_JombayExam_NewInterviewMaster.arranged_By,tbl_JombayExam_NewInterviewMaster.created_At, tbl_JombayExam_NewInterviewMaster.type,  tbl_JombayExam_NewInterviewMaster.Experience,tbl_JombayExam_NewInterviewMaster.isAccepted,tbl_JombayExam_NewInterviewMaster.statusBy,tbl_JombayExam_NewInterviewMaster.statusAddedAt,tbl_Location_Master.location_Name, tbl_Position_Master.position_Name, tbl_Department_Master.Department_Name,(select top 1 pending_assessment_count from tbl_AssessmentTest_Data where Candidate_Email= tbl_JombayExam_NewInterviewMaster.User_Email order by inserted_at desc ) as pendingExam FROM tbl_JombayExam_NewInterviewMaster INNER JOIN  tbl_Department_Master ON tbl_JombayExam_NewInterviewMaster.department_Id = tbl_Department_Master.Department_Id INNER JOIN                         tbl_Location_Master ON tbl_JombayExam_NewInterviewMaster.location_Id = tbl_Location_Master.location_Id INNER JOIN                         tbl_Position_Master ON tbl_JombayExam_NewInterviewMaster.position_Id = tbl_Position_Master.position_Id where tbl_JombayExam_NewInterviewMaster.User_Email='" + userEmail + "' order by  created_At desc";


            DataTable dtDetails = DBUtils.SQLSelect(new SqlCommand(query));
            foreach ( DataRow dr in dtDetails.Rows)
            {
                allContents = allContents.Replace("@#User_Name#@", Convert.ToString(dr["User_Name"]));

                allContents = allContents.Replace("@#User_Email#@", Convert.ToString(dr["User_Email"]));

                allContents = allContents.Replace("@#location_Name#@", Convert.ToString(dr["location_Name"]));

                allContents = allContents.Replace("@#position_Name#@", Convert.ToString(dr["position_Name"]));

                allContents = allContents.Replace("@#Department_Name#@", Convert.ToString(dr["Department_Name"]));

                allContents = allContents.Replace("@#Experience#@", Convert.ToString(dr["Experience"]));

                allContents = allContents.Replace("@#RESULT@#", Convert.ToString(dr["isAccepted"]));


                allContents = allContents.Replace("@#arranged_By#@", Convert.ToString(dr["arranged_By"]));

                allContents = allContents.Replace("@#RESULT@#", Convert.ToString(dr["isAccepted"]));

                  allContents = allContents.Replace("@#Pending_Exam@#", Convert.ToString(dr["pendingExam"]));


                if (Convert.ToString(dr["isAccepted"]).Equals(ACCEPTED))
                {

                    allContents = allContents.Replace("@#RESULT_COLOR@#", "lightgreen");
                    
                }
                else if (Convert.ToString(dr["isAccepted"]).Equals(REJECTED))
                {
                    allContents = allContents.Replace("@#RESULT_COLOR@#", "indianred");
                }



              

            }






        body = allContents;
        return body;

    }


        private void fnCreateLog(string message, string fileName)
        {
            string path = Application.StartupPath + @"\Config\log\" + fileName;


            bool exists = System.IO.Directory.Exists(Application.StartupPath + @"\Config\log");

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(Application.StartupPath + @"\Config\log");
            }

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();

                using (TextWriter tw = new StreamWriter(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                }

            }
            else if (File.Exists(path))
            {

                using (StreamWriter tw = File.AppendText(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                    tw.Close();
                }


            }




        }


        private void fnCreateCandidate_LogHistory(string message, string fileName)
        {
            string path = Application.StartupPath + @"\Config\Candidate_Log\" + fileName;


            bool exists = System.IO.Directory.Exists(Application.StartupPath + @"\Config\Candidate_Log");

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(Application.StartupPath + @"\Config\Candidate_Log");
            }

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();

                using (TextWriter tw = new StreamWriter(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                }

            }
            else if (File.Exists(path))
            {

                using (StreamWriter tw = File.AppendText(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                    tw.Close();
                }


            }




        }





        private DataTable GenerateTransposedTable(DataTable inputTable, string gridType)
        {
            DataTable outputTable = new DataTable();

          
            if (gridType.Equals("Exam"))
            {
                outputTable.Columns.Add("Test");
                outputTable.Columns.Add("Data");
            }
            if (gridType.Equals("HR"))
            {
                outputTable.Columns.Add("Question");
                outputTable.Columns.Add("Answer");
            }


            // Add rows by looping columns        
            for (int rCount = 0; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }

    }
}
