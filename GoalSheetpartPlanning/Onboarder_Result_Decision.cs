﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoalSheetpartPlanning
{
    public partial class Onboarder_Result_Decision : Form
    {
        public string HR = "HR";
        public string CORE_TEAM = "CORE TEAM";

        public string ACCEPTED = "Accepted";
        public string REJECTED = "Rejected";

        public string fname = string.Empty;


        DataTable dt_candidateResult;

        public Onboarder_Result_Decision()
        {
            InitializeComponent();
        }

        private void Onboarder_Result_Decision_Load(object sender, EventArgs e)
        {

            try
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();

               

                labelStatus.Text = "Action Started...";
                Application.DoEvents(); 


                string json = string.Empty;
                string qry = "select Candidate_Email,[competencies_array],user_assessment_id from [tbl_AssessmentTest_Data] where [syncUpdated] IS NULL ";
                DataTable dt = DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qry));

                if (dt.Rows.Count > 0)
                {

                    foreach (DataRow dr in dt.Rows)
                    {
                       

                        string candidate_email = Convert.ToString(dr["Candidate_Email"]);
                        string candidate_user_assessment_id = Convert.ToString(dr["user_assessment_id"]);

                        fname = candidate_email + "_" + candidate_user_assessment_id + ".txt";






                        //Getting Position and Position type
                        DataTable dt_candidate_details = fnGetCandidateDetails(candidate_email);

                        if (dt_candidate_details.Rows.Count > 0)
                        {
                            json = "[ " + Convert.ToString(dr["competencies_array"]) + " ] ";

                            string position = dt_candidate_details.Rows[0]["position"].ToString();
                            string exam_type = dt_candidate_details.Rows[0]["exam_type"].ToString();
                            DataTable tester = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));
                            DataTable competencies_result = GenerateTransposedTable(tester);


                            string BatchNo= dt_candidate_details.Rows[0]["BatchNo"].ToString();

                            string message = "Json array: " + json;
                            fnCreateLog(message, fname);

                            message = "position: " + position + " ; exam_type :" + exam_type;
                            fnCreateLog(message, fname);






                            string finalResult = fnGetFinalResult(competencies_result, position, exam_type, out dt_candidateResult);


                            message = candidate_email + " Is " + finalResult ;
                            fnCreateLog(message, fname);


                            foreach (DataRow item in dt_candidateResult.Rows)
                            {
                                fnInsertCandidateResult(candidate_email, candidate_user_assessment_id, Convert.ToString(item["Skills"]), Convert.ToString(item["ExpectedResult"]), Convert.ToString(item["ExpectedResult_score"]), Convert.ToString(item["ActualResult"]), Convert.ToString(item["ActualResult_score"]), Convert.ToString(item["isAccepted"]), Convert.ToString(item["isMandatory"]));


                            }

                            dt_candidateResult.Rows.Clear();

                            if (string.IsNullOrEmpty(finalResult))
                            {
                               
                            }
                            else
                            {

                                fnUpdateCandidateRes(finalResult, candidate_email, BatchNo);
                            }






                            //check if person fits for any other position
                            if (finalResult == CommonMethods.rejected)
                            {


                              


                                string qr = "select Department_Name  from tbl_Department_Master where isActive='True'";
                                DataTable dt_postion = DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qr));


                                ArrayList al_candidateFitFor = new ArrayList();

                                foreach (DataRow item in dt_postion.Rows)
                                {
                                    string candidateResult = fnGetFinalResult(competencies_result, Convert.ToString(item["Department_Name"]), exam_type, out dt_candidateResult);

                                    if (candidateResult == CommonMethods.accepted)
                                    {

                                      



                                        al_candidateFitFor.Add(Convert.ToString(item["Department_Name"]));
                                    }


                                }




                                var strings = from object o in al_candidateFitFor
                                              select o.ToString();

                                var suggestedPosition = string.Join(";", strings.ToArray());

                                message = "Accepted for " + suggestedPosition;
                                fnCreateLog(message, fname); 

                                fnInsertSuggestedPosition(candidate_email, candidate_user_assessment_id, position, finalResult, suggestedPosition.ToString().TrimEnd(';'), exam_type);

                            }



                        }
                    }


                    watch.Stop();
                    var elapsedMs = watch.ElapsedMilliseconds;
                    labelStatus.Text = "Action complete in (milisec) ..." + Convert.ToString(elapsedMs);
                    Application.Exit();
                }

                else
                {
                    watch.Stop();
                    var elapsedMs = watch.ElapsedMilliseconds;


                    fnCheckDataSyncOr_Not();

                   
                    fnCreateLog("data not found to sync", DateTime.Today.ToString("dd_MMM_yyyy")+".txt");

                    labelStatus.Text = "data not found to sync,Action Complete in (milisec) : " + Convert.ToString(elapsedMs);
                    Application.Exit();

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void fnUpdateCandidateRes(string res, string userEmail,string BatchNo)
        {
            try
            {


                string updateStatus_qry = "UPDATE [tbl_JombayExam_NewInterviewMaster] SET [isAccepted] = '" + res + "'  ,statusBy ='SYSTEM',statusAddedAt ='" + DateTime.Now.ToString("dd/MMM/yyy HH:mm") + "' WHERE [User_Email] = '" + userEmail + "'  and  [isAccepted] is null";

                int i = DBUtils.ExecuteSQLCommand(new System.Data.SqlClient.SqlCommand(updateStatus_qry));

                labelStatus.Text = "Candidate Result Updated...";
                Application.DoEvents();

               

                if (i > 0)
                {
                    string message = "Result Updated at  tbl_JombayExam_NewInterviewMaster : " + userEmail;
                    fnCreateLog(message, fname);


                    fnSendMail(userEmail, BatchNo);
                    fnUpdateSync(userEmail);

                }
                else
                {
                    string message = "Failed to  Updated at  tbl_JombayExam_NewInterviewMaster : " + userEmail;
                    fnCreateLog(message, fname);


                }


            }
            catch (Exception ex)
            {
                string message = ex.ToString()+  userEmail;
                fnCreateLog(message, fname);
                throw ex;
            }
        }


        private void fnUpdateSync(string userEmail)
        {
            try
            {
                string qry = "UPDATE [tbl_AssessmentTest_Data] SET [syncUpdated] = 'True' ,[syncUpdatedAt]='" + Convert.ToString(DateTime.Now) + "'  WHERE [Candidate_Email]='" + userEmail + "'";

                int i = DBUtils.ExecuteSQLCommand(new SqlCommand(qry));

                labelStatus.Text = "Test Sync details updated...";

                if (i > 0)
                {
                    string message = "Test Sync details updated..." + "---------- " + userEmail;
                    fnCreateLog(message, fname);

                    Application.DoEvents();
                }
             

            }
            catch (Exception ex)
            {
                string message =ex.ToString() + "---------- " + userEmail;
                fnCreateLog(message, fname);

                //MessageBox.Show(ex.ToString());
            }

        }

        private void fnSendMail(string userEmail,string BatchNo)
        {
            try
            {
                string emailId = string.Empty;
                string password = string.Empty;
                SmtpClient smtp = new SmtpClient();
                smtp.Timeout = 2 * 300000;
                string qry = "SELECT * FROM tblSMTPSettingsMaster";
                DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));
                foreach (DataRow dr in dt.Rows)
                {
                    smtp.Host = dr["Servername"].ToString();
                    smtp.Port = Convert.ToInt16(dr["Portno"].ToString());
                    smtp.EnableSsl = Convert.ToBoolean(dr["EnableSsl"].ToString());
                    emailId = dr["Emailid"].ToString();
                    password = dr["Usrpassword"].ToString();
                }
                smtp.Credentials = new System.Net.NetworkCredential(emailId, password);


                System.Net.Mail.MailMessage mail1 = new System.Net.Mail.MailMessage();

                mail1.From = new MailAddress(emailId, "Do Not Reply");
                mail1.Subject = "Result updated By system!";

                string qryToEmail = string.Empty;
                if (!string.IsNullOrEmpty(BatchNo))
                {

                    qryToEmail = "select [hr_Email] FROM [tbl_User_Access] where Role='CORE TEAM' ";
                }
                else {
                    qryToEmail = "select [hr_Email] FROM [tbl_User_Access]";
                }

                DataTable dt_To = DBUtils.SQLSelect(new SqlCommand(qryToEmail));
                foreach (DataRow dr in dt_To.Rows)
                {
                    mail1.Bcc.Add(Convert.ToString(dr["hr_Email"]));
                }

             



                mail1.IsBodyHtml = true;
                string bodyUser = fnFetchEMailBody(userEmail);
                mail1.Body = bodyUser;
                smtp.Send(mail1);



                string message = "Mail sent to HR" +  userEmail;
                fnCreateLog(message, fname);

                labelStatus.Text = "Mail Sent to HR...";
                Application.DoEvents();

              



            }
            catch (Exception ex)
            {
                string message = ex.ToString()  + "---------- " + userEmail;
                fnCreateLog(message, fname);

                //throw ex;
            }


        }


        protected string fnFetchEMailBody(string userEmail)
        {


            string body, name = "", email = "";


            string binaryPath =
     System.IO.Path.GetDirectoryName(
         System.Reflection.Assembly.GetEntryAssembly().Location) + "\\candidateDetails.html";

            //To read html file
            StreamReader readTemplateFile = null;



            readTemplateFile = new StreamReader(binaryPath);

            string allContents = readTemplateFile.ReadToEnd();


            string query = "SELECT TOP 1 tbl_JombayExam_NewInterviewMaster.Id_Exam,tbl_JombayExam_NewInterviewMaster.User_Email,tbl_JombayExam_NewInterviewMaster.User_Name,tbl_JombayExam_NewInterviewMaster.age,tbl_JombayExam_NewInterviewMaster.remark,tbl_JombayExam_NewInterviewMaster.arranged_By,tbl_JombayExam_NewInterviewMaster.created_At, tbl_JombayExam_NewInterviewMaster.type,  tbl_JombayExam_NewInterviewMaster.Experience,tbl_JombayExam_NewInterviewMaster.isAccepted,tbl_JombayExam_NewInterviewMaster.statusBy,tbl_JombayExam_NewInterviewMaster.statusAddedAt,tbl_Location_Master.location_Name, position_type as position_Name, tbl_Department_Master.Department_Name,(select top 1 pending_assessment_count from tbl_AssessmentTest_Data  order by inserted_at desc ) as pendingExam FROM tbl_JombayExam_NewInterviewMaster INNER JOIN  tbl_Department_Master ON tbl_JombayExam_NewInterviewMaster.department_Id = tbl_Department_Master.Department_Id INNER JOIN                         tbl_Location_Master ON tbl_JombayExam_NewInterviewMaster.location_Id = tbl_Location_Master.location_Id INNER JOIN                         tbl_Position_Master ON tbl_JombayExam_NewInterviewMaster.position_Id = tbl_Position_Master.position_Id where tbl_JombayExam_NewInterviewMaster.User_Email='" + userEmail + "' order by  created_At desc";


            DataTable dtDetails = DBUtils.SQLSelect(new SqlCommand(query));
            foreach (DataRow dr in dtDetails.Rows)
            {
                allContents = allContents.Replace("@#User_Name#@", Convert.ToString(dr["User_Name"]));

                allContents = allContents.Replace("@#User_Email#@", Convert.ToString(dr["User_Email"]));

                allContents = allContents.Replace("@#location_Name#@", Convert.ToString(dr["location_Name"]));

                allContents = allContents.Replace("@#position_Name#@", Convert.ToString(dr["position_Name"]));

                allContents = allContents.Replace("@#Department_Name#@", Convert.ToString(dr["Department_Name"]));

                allContents = allContents.Replace("@#Experience#@", Convert.ToString(dr["Experience"]));

                allContents = allContents.Replace("@#RESULT@#", Convert.ToString(dr["isAccepted"]));


                allContents = allContents.Replace("@#arranged_By#@", Convert.ToString(dr["arranged_By"]));

                allContents = allContents.Replace("@#RESULT@#", Convert.ToString(dr["isAccepted"]));

                allContents = allContents.Replace("@#Pending_Exam@#", Convert.ToString(dr["pendingExam"]));


                if (Convert.ToString(dr["isAccepted"]).Equals(ACCEPTED))
                {

                    allContents = allContents.Replace("@#RESULT_COLOR@#", "lightgreen");

                }
                else if (Convert.ToString(dr["isAccepted"]).Equals(REJECTED))
                {
                    allContents = allContents.Replace("@#RESULT_COLOR@#", "indianred");
                }





            }






            body = allContents;
            return body;

        }

        private void fnCheckDataSyncOr_Not()
        {
            try
            {
                //Check > candidate completed exam
                string qry1 = "select * ,(select top 1 tool_status from [tbl_Incompleted_AssessmentTest_Data] where [Candidate_Email] =  tbl_JombayExam_NewInterviewMaster.[User_Email]) as tool_status from tbl_JombayExam_NewInterviewMaster where ( [User_Email] In ( SELECT [Candidate_Email] from  [tbl_Incompleted_AssessmentTest_Data]) And [User_Email] NOT IN ( SELECT [Candidate_Email] from  tbl_AssessmentTest_Data))  and (DATEDIFF(HOUR,created_At,GETDATE()) >= " + CommonMethods.fnFromTime() + " and   DATEDIFF(HOUR,created_At,GETDATE()) <= " + CommonMethods.fnToTime() + ")";

                DataTable dt1 = DBUtils.SQLSelect(new SqlCommand(qry1));

                if (dt1.Rows.Count > 0)
                {

                    foreach (DataRow item in dt1.Rows)
                    {
                        fnSend_Email_ExamIncompleted(Convert.ToString(item["User_Email"]), Convert.ToString(item["User_Name"]), Convert.ToDateTime(item["created_At"]).ToString("dd/MMM/yyy HH:MM"), Convert.ToString(item["tool_status"]));

                    }

                }
                else
                {
                    //If not then issue with sync from jombay


                    string qry = "select * ,DATEDIFF(HOUR,created_At,GETDATE()) from tbl_JombayExam_NewInterviewMaster where User_Email NOT IN (select Candidate_Email from tbl_AssessmentTest_Data where syncUpdated = 'True')  and (DATEDIFF(HOUR,created_At,GETDATE()) >= " + CommonMethods.fnFromTime() + " and   DATEDIFF(HOUR,created_At,GETDATE()) <= " + CommonMethods.fnToTime() + ")";


                    DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            fnSend_Email_DataNotReceived(Convert.ToString(item["User_Email"]), Convert.ToString(item["User_Name"]), Convert.ToDateTime(item["created_At"]).ToString("dd/MMM/yyy HH:MM"));
                        }
                    }
                }


            }
            catch (Exception ex)
            {
            
                throw ex;
            }
        }
        private void fnSend_Email_DataNotReceived(string email, string name, string createdAt)
        {
            try
            {
                try
                {
                    string emailId = string.Empty;
                    string password = string.Empty;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Timeout = 2 * 300000;
                    string qry = "SELECT * FROM tblSMTPSettingsMaster";
                    DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));
                    foreach (DataRow dr in dt.Rows)
                    {
                        smtp.Host = dr["Servername"].ToString();
                        smtp.Port = Convert.ToInt16(dr["Portno"].ToString());
                        smtp.EnableSsl = Convert.ToBoolean(dr["EnableSsl"].ToString());
                        emailId = dr["Emailid"].ToString();
                        password = dr["Usrpassword"].ToString();
                    }
                    smtp.Credentials = new System.Net.NetworkCredential(emailId, password);


                    System.Net.Mail.MailMessage mail1 = new System.Net.Mail.MailMessage();

                    mail1.From = new MailAddress(emailId, "Do Not Reply");
                    mail1.Subject = "Result not received!";

                    string qryToEmail = "select [hr_Email] FROM [tbl_User_Access] where Role = '" + HR + "' ";
                    DataTable dt_To = DBUtils.SQLSelect(new SqlCommand(qryToEmail));
                    foreach (DataRow dr in dt_To.Rows)
                    {
                        mail1.Bcc.Add(Convert.ToString(dr["hr_Email"]));
                    }


                    string jombayemail_list = CommonMethods.fnGetJombayTeamEmail();
                    string[] jombayEmail = jombayemail_list.Split(';');


                    foreach (string id in jombayEmail)
                    {
                        if (!string.IsNullOrEmpty(id))
                        {
                            mail1.Bcc.Add(id);
                        }
                    }

                    mail1.IsBodyHtml = true;
                    string bodyUser = fnFetchEMailBody_ResultNotFetched(email, name, createdAt);
                    mail1.Body = bodyUser;
                    smtp.Send(mail1);

                    labelStatus.Text = "Mail Sent to HR- Data not received...";
                    Application.DoEvents();

                 



                }
                catch (Exception ex)
                {
                   

                    //throw ex;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void fnCreateLog(string message, string fileName)
        {
            string path = Application.StartupPath + @"\Config\log\" + fileName;


            bool exists = System.IO.Directory.Exists(Application.StartupPath + @"\Config\log");

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(Application.StartupPath + @"\Config\log");
            }

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();

                using (TextWriter tw = new StreamWriter(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                }

            }
            else if (File.Exists(path))
            {

                using (StreamWriter tw = File.AppendText(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                    tw.Close();
                }


            }




        }
        private string fnFetchEMailBody_ResultNotFetched(string email, string name, string createdAt)
        {
            try
            {
                string body;


                string binaryPath =
         System.IO.Path.GetDirectoryName(
             System.Reflection.Assembly.GetEntryAssembly().Location) + "\\ResultNotSync.html";

                //To read html file
                StreamReader readTemplateFile = null;



                readTemplateFile = new StreamReader(binaryPath);

                string allContents = readTemplateFile.ReadToEnd();


                allContents = allContents.Replace("@#User_Name#@", name);
                allContents = allContents.Replace("@#User_Email#@", email);
                allContents = allContents.Replace("@#Created_At#@", createdAt);


                body = allContents;
                return body;
            }
            catch (Exception ex)
            {
             
                throw ex;
            }
        }

        private DataTable fnGetCandidateDetails(string candidate_email)
        {
            try
            {
                string qry = "SELECT TOP 1 (select UPPER([Department_Name]) from [tbl_Department_Master] where [Department_Id]=MT.[Department_Id]) as position,UPPER(position_type) as exam_type,BatchNo   FROM [tbl_JombayExam_NewInterviewMaster] as MT where MT.User_Email='" + candidate_email + "'";
                return DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qry));



            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();

            // Add columns by looping rows

            // Header row's first column is same as in inputTable
            //outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

            // Header row's second column onwards, 'inputTable's first column taken
            //foreach (DataRow inRow in inputTable.Rows)
            //{
            //    string newColName = inRow[0].ToString();
            //    outputTable.Columns.Add(newColName);
            //}


            outputTable.Columns.Add("Skill");
            outputTable.Columns.Add("Result");



            // Add rows by looping columns        
            for (int rCount = 0; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }


        private string fnGetFinalResult(DataTable competencies_result, string position, string exam_type, out DataTable dt_candidateResult)
        {


           

           
           
           

          

            Boolean isSkillMatrixPresent = fnCheck_SkillMatrixPresent(position, exam_type);


          



            if (isSkillMatrixPresent)
            {

                string message = "Skill matrix present for  position : " +  position+  " and exam_type : " + exam_type;
            fnCreateLog(message, fname);


                Boolean isRejected = false;


                DataTable dt_final = new DataTable();
                dt_final.Clear();
                dt_final.Columns.Add("Skills");
                dt_final.Columns.Add("ExpectedResult");
                dt_final.Columns.Add("ExpectedResult_score");
                dt_final.Columns.Add("ActualResult");
                dt_final.Columns.Add("ActualResult_score");
                dt_final.Columns.Add("isAccepted");

                dt_final.Columns.Add("isMandatory");



                foreach (DataRow dr in competencies_result.Rows)
                {

                   

                   



                    string skill = Convert.ToString(dr["Skill"]);


                    



                    //get detals for position and type 
                    DataTable dt_tempDetails = fnGetSkillDataFromDb(skill, position, exam_type);

                    string expectedResult = string.Empty;
                    Boolean isMandatory = false;

                    if (dt_tempDetails.Rows.Count > 0)
                    {
                        expectedResult = Convert.ToString(dt_tempDetails.Rows[0]["level_name"]);
                        isMandatory = Convert.ToBoolean(dt_tempDetails.Rows[0]["isMandatory"]);
                    }

                    string actualResult = Convert.ToString(dr["Result"]);

                    string isAccepted = string.Empty;


                    string exp_res_priority = fnGetResultPriority(expectedResult.ToUpper());
                    string act_Res_priority = fnGetResultPriority(actualResult.ToUpper());







                    if (exp_res_priority == "0")
                    {
                        isAccepted = "";
                    }

                    else if (Convert.ToInt32(act_Res_priority) >= Convert.ToInt32(exp_res_priority))
                    {
                        isAccepted = CommonMethods.accepted;
                    }
                    else
                    {
                        isAccepted = CommonMethods.rejected;
                        if (isMandatory)
                        {
                            isRejected = true;

                        }
                    }




                   


                    dt_final.Rows.Add(skill, expectedResult, exp_res_priority, actualResult, act_Res_priority, isAccepted, Convert.ToString(isMandatory));



                }
                dt_candidateResult = dt_final;

                if (isRejected)
                {



                 



                    return CommonMethods.rejected;
                }
                else
                {
                    return CommonMethods.accepted;
                }



            }
            else
            {

                string message = "Skill matrix is Empty for  position : " + position + " and exam_type : " + exam_type;
                fnCreateLog(message, fname);
                dt_candidateResult = null;
                return "Incomplete Skill Matrix";
            }


        }

        private bool fnCheck_SkillMatrixPresent(string position, string type)
        {
            try
            {
                string qry = "select count(*) from tbl_Result_Decision_Matrix where UPPER(position_name)='" + position + "' and UPPER( exam_type)='" + type + "'";

                string res = DBUtils.SqlSelectScalar(new System.Data.SqlClient.SqlCommand(qry));

              

                if (Convert.ToInt32(res) == 0)
                {

                  

                  

                    return false;
                }
                else
                {
                   

                    
                    return true;
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private DataTable fnGetSkillDataFromDb(string skill, string position, string exam_type)
        {
            try
            {
                string qry = "select skill_name,level_name, isMandatory from [tbl_Result_Decision_Matrix] where [position_Name]='" + position + "' and exam_type='" + exam_type + "' and skill_name='" + skill + "'";
                return DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qry));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private string fnGetResultPriority(string result)
        {
            try
            {
                string qry = "select level_priority FROM [tbl_Level_Master] where level_name = '" + result + "'";

                string result1 = DBUtils.SqlSelectScalar(new System.Data.SqlClient.SqlCommand(qry));


                if (string.IsNullOrEmpty(result1))
                {
                    string message = "Level is not present " + result;
                    fnCreateLog(message, fname);


                    return "0";
                }
                else
                {
                    return result1;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private void fnInsertSuggestedPosition(string email, string assesment_id, string applied_position, string applied_position_result, string suggested_position,string type)
        {
            try
            {
                string qry = "INSERT INTO [tbl__Reject_Candidates_Suggested_position] ([candidate_email],[User_Assessment_Id],[applied_position],[actual_result],[system_position_suggestion] ,[inserted_at],exam_type) VALUES  ('" + email + "','" + assesment_id + "','" + applied_position + "','" + applied_position_result + "','" + suggested_position + "','" + DateTime.Now.ToString("dd MMM yyyy HH:mm") + "','"+ type + "')";
                int i = DBUtils.ExecuteSQLCommand(new System.Data.SqlClient.SqlCommand(qry));
                if (i > 0)
                {
                   string message = "inserted into  tbl__Reject_Candidates_Suggested_position " + suggested_position;
                    fnCreateLog(message, fname);
                }
                else {
                    string message = "failed to insert into  tbl__Reject_Candidates_Suggested_position " + suggested_position;
                    fnCreateLog(message, fname);
                }

              

            }
            catch (Exception ex)
            {
                string message = ex.ToString() + suggested_position;
                fnCreateLog(message, fname);
                throw ex;
            }
        }

        private void fnInsertCandidateResult(string email, string user_assesment_id, string skill, string expected_result, string expected_result_score, string actual_result, string actual_result_score, string is_accepted, string is_isMandatory)
        {
            try
            {
                string qry = "INSERT INTO [tbl_System_Competencies_Result] ([Candidate_Email],[User_Assessment_Id],[Skill] ,[ExpectedResult],[ExpectedResult_score],[ActualResult] ,[ActualResult_score],[isAccepted],[isMandatory],inserted_at) VALUES ('" + email + "','" + user_assesment_id + "','" + skill + "','" + expected_result + "','" + expected_result_score + "','" + actual_result + "','" + actual_result_score + "','" + is_accepted + "','" + is_isMandatory + "','" + DateTime.Now.ToString("dd MMM yyyy HH:mm") + "')";
                int i = DBUtils.ExecuteSQLCommand(new System.Data.SqlClient.SqlCommand(qry));


               
            }
            catch (Exception ex)
            {
                string message = ex.ToString();
                fnCreateLog(message, fname);
                throw ex;
            }
        }





        private void fnCreateCandidate_LogHistory(string message, string fileName)
        {
            string path = Application.StartupPath + @"\Config\Candidate_Log\" + fileName;


            bool exists = System.IO.Directory.Exists(Application.StartupPath + @"\Config\Candidate_Log");

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(Application.StartupPath + @"\Config\Candidate_Log");
            }

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();

                using (TextWriter tw = new StreamWriter(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                }

            }
            else if (File.Exists(path))
            {

                using (StreamWriter tw = File.AppendText(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                    tw.Close();
                }


            }




        }


        private void fnSend_Email_ExamIncompleted(string userEmail, string userName, string createdAt, string examStatus)
        {
            try
            {
                try
                {
                    string emailId = string.Empty;
                    string password = string.Empty;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Timeout = 2 * 300000;
                    string qry = "SELECT * FROM tblSMTPSettingsMaster";
                    DataTable dt = DBUtils.SQLSelect(new SqlCommand(qry));
                    foreach (DataRow dr in dt.Rows)
                    {
                        smtp.Host = dr["Servername"].ToString();
                        smtp.Port = Convert.ToInt16(dr["Portno"].ToString());
                        smtp.EnableSsl = Convert.ToBoolean(dr["EnableSsl"].ToString());
                        emailId = dr["Emailid"].ToString();
                        password = dr["Usrpassword"].ToString();
                    }
                    smtp.Credentials = new System.Net.NetworkCredential(emailId, password);


                    System.Net.Mail.MailMessage mail1 = new System.Net.Mail.MailMessage();

                    mail1.From = new MailAddress(emailId, "Do Not Reply");
                    mail1.Subject = "Exam Incomplete!";

                    string qryToEmail = "select [hr_Email] FROM [tbl_User_Access] where Role = '" + HR + "' ";
                    DataTable dt_To = DBUtils.SQLSelect(new SqlCommand(qryToEmail));
                    foreach (DataRow dr in dt_To.Rows)
                    {
                        mail1.Bcc.Add(Convert.ToString(dr["hr_Email"]));
                    }





                    mail1.IsBodyHtml = true;
                    string bodyUser = fnFetchEMailBody_ExamIncompleted(userEmail, userName, createdAt, examStatus);
                    mail1.Body = bodyUser;
                    smtp.Send(mail1);

                    labelStatus.Text = "Mail Sent to HR-Exam Incomplete...";
                    Application.DoEvents();

                  



                }
                catch (Exception ex)
                {
                  

                    //throw ex;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        private string fnFetchEMailBody_ExamIncompleted(string userEmail, string userName, string createdAt, string examStatus)
        {
            try
            {
                string body;


                string binaryPath =
         System.IO.Path.GetDirectoryName(
             System.Reflection.Assembly.GetEntryAssembly().Location) + "\\ExamIncomplete.html";

                //To read html file
                StreamReader readTemplateFile = null;



                readTemplateFile = new StreamReader(binaryPath);

                string allContents = readTemplateFile.ReadToEnd();


                allContents = allContents.Replace("@#User_Name#@", userName);
                allContents = allContents.Replace("@#User_Email#@", userEmail);
                allContents = allContents.Replace("@#Created_At#@", createdAt);


                string json = "[ " + examStatus + " ] ";

                if (!string.IsNullOrEmpty(examStatus))


                {
                    DataTable tester = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));

                    DataTable dtClone = tester.Clone(); //just copy structure, no data
                    for (int i = 0; i < dtClone.Columns.Count; i++)
                    {
                        if (dtClone.Columns[i].DataType != typeof(string))
                            dtClone.Columns[i].DataType = typeof(string);
                    }

                    foreach (DataRow dr in tester.Rows)
                    {
                        dtClone.ImportRow(dr);
                    }

                    DataTable transposedData = GenerateTransposedTable(dtClone, "Exam");

                    DataTable dt_New = new DataTable();
                    dt_New.Clear();
                    dt_New.Columns.Add("Test");
                    dt_New.Columns.Add("Result");
                    foreach (DataRow dr in transposedData.Rows)
                    {
                        string res = string.Empty;
                        String testName = Convert.ToString(dr["Test"]);



                        testName = Convert.ToString(dr["Test"]).Replace("_", " ");
                        testName = testName.Replace("is", " ");



                        if (Convert.ToString(dr["Data"]).Equals("True"))
                        {
                            res = "Completed";
                        }
                        if (Convert.ToString(dr["Data"]).Equals("False"))
                        {
                            res = "Pending";
                        }

                        dt_New.Rows.Add(testName, res);
                    }


                    StringBuilder sb_examStatus = new StringBuilder();




                    foreach (DataRow item in dt_New.Rows)
                    {



                        sb_examStatus.Append(@"<tr> <td>" + Convert.ToString(item["Test"]) + "</td><td style='font - weight: bold;' >" + Convert.ToString(item["Result"]) + "</td> </ tr > ");






                    }


                    allContents = allContents.Replace("@#Exam_Details#@", sb_examStatus.ToString());

                }

                body = allContents;
                return body;
            }
            catch (Exception ex)
            {
              
                throw ex;
            }
        }


        private DataTable GenerateTransposedTable(DataTable inputTable, string gridType)
        {
            DataTable outputTable = new DataTable();


            if (gridType.Equals("Exam"))
            {
                outputTable.Columns.Add("Test");
                outputTable.Columns.Add("Data");
            }
            if (gridType.Equals("HR"))
            {
                outputTable.Columns.Add("Question");
                outputTable.Columns.Add("Answer");
            }


            // Add rows by looping columns        
            for (int rCount = 0; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }
    }
}
