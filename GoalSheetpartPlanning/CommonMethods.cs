﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;


namespace GoalSheetpartPlanning
{
    class CommonMethods
    {

        public static string accepted = "Accepted";
        public static string rejected = "Rejected";

        public static string fnConnectionString()
        {
            try
            {
                string path = string.Empty;
                string xmldoc = string.Empty;
                string ans = string.Empty;
                path = Application.StartupPath + @"\Config\Config.xml";
                xmldoc = File.ReadAllText(path);
                XmlReader reader = XmlReader.Create(new StringReader(xmldoc));
                try
                {
                    reader.ReadToFollowing("Settings");
                    reader.MoveToNextAttribute();
                    reader.ReadToFollowing("ConnectionString");
                    ans = reader.ReadElementContentAsString();
                    return ans;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static string fnGetJombayTeamEmail()
        {
            try
            {
                string path = string.Empty;
                string xmldoc = string.Empty;
                string ans = string.Empty;
                path = Application.StartupPath + @"\Config\Config.xml";
                xmldoc = File.ReadAllText(path);
                XmlReader reader = XmlReader.Create(new StringReader(xmldoc));
                try
                {
                    reader.ReadToFollowing("Settings");
                    reader.MoveToNextAttribute();
                    reader.ReadToFollowing("jombay_team_email");
                    ans = reader.ReadElementContentAsString();
                    return ans;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static string fnFromTime()
        {
            try
            {
                string path = string.Empty;
                string xmldoc = string.Empty;
                string ans = string.Empty;
                path = Application.StartupPath + @"\Config\Config.xml";
                xmldoc = File.ReadAllText(path);
                XmlReader reader = XmlReader.Create(new StringReader(xmldoc));
                try
                {
                    reader.ReadToFollowing("Settings");
                    reader.MoveToNextAttribute();
                    reader.ReadToFollowing("sendMailTime_From");
                    ans = reader.ReadElementContentAsString();
                    return ans;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string fnToTime()
        {
            try
            {
                string path = string.Empty;
                string xmldoc = string.Empty;
                string ans = string.Empty;
                path = Application.StartupPath + @"\Config\Config.xml";
                xmldoc = File.ReadAllText(path);
                XmlReader reader = XmlReader.Create(new StringReader(xmldoc));
                try
                {
                    reader.ReadToFollowing("Settings");
                    reader.MoveToNextAttribute();
                    reader.ReadToFollowing("sendMailTime_To");
                    ans = reader.ReadElementContentAsString();
                    return ans;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }






        public static string fnCreateLog(string message)
        {

            String fileName = "Dbutils_" + DateTime.Now.ToString("dd-MMM-yyyy") + ".txt";

            string path = Application.StartupPath + @"\Config\" + fileName;


            //string path = @"E:\AppServ\Example.txt";

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();

                using (TextWriter tw = new StreamWriter(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                }

            }
            else if (File.Exists(path))
            {

                using (StreamWriter tw = File.AppendText(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                    tw.Close();
                }


            }

            return "Success";


        }

    }
}
