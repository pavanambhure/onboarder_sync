﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoalSheetpartPlanning
{
   
    public partial class Form1 : Form
    {

        int inserted_Cnt = 0;

        int insert_Cnt_day = 0;
        private delegate void UpdateStatusDelegate(string status);


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void UpdateStatus(string status)
        {
            if (this.lblStatus.InvokeRequired)
            {
                this.Invoke(new UpdateStatusDelegate(this.UpdateStatus), new object[] { status });
                return;
            }

            this.lblStatus.Text = status;
        }
        private void button1_Click(object sender, EventArgs e)
        {

            button1.Visible = false;
            btnDayWise.Visible = false;
            int res = fnDeleteAllRecord();


            if (res == 1)
            {

                string qry = "select * from GOAL_SHEET_PART_PLANNING ";
                DataTable details_All = DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qry));

                string filename = "Month_" + DateTime.Now.ToString("dd-MMM-yyyy HH-MM") + ".txt";

                foreach (DataRow dr in details_All.Rows)
                {



                    string Sales_Plant = Convert.ToString(dr["Sales_Plant"]);
                    string Category = Convert.ToString(dr["Category"]);
                    string Segment = Convert.ToString(dr["Segment"]);
                    string Cust_Code = Convert.ToString(dr["Cust_Code"]);
                    string Cust_Group = Convert.ToString(dr["Cust_Group"]);
                    string Cust_Name = Convert.ToString(dr["Cust_Name"]);
                    string Type = Convert.ToString(dr["Type"]);
                    string Prod_IPD = Convert.ToString(dr["Prod_IPD"]);
                    string Material_Code = Convert.ToString(dr["Material_Code"]);
                    string Material_Description = Convert.ToString(dr["Material_Description"]);
                    string Cust_Part = Convert.ToString(dr["Cust_Part"]);
                    string Existing_Rate = Convert.ToString(dr["Existing_Rate"]);
                    string Currency = Convert.ToString(dr["Currency"]);
                    string Rate_Per = Convert.ToString(dr["Rate_Per"]);
                    string Rate_Unit = Convert.ToString(dr["Rate_Unit"]);
                    string Financial_Year = Convert.ToString(dr["Financial_Year"]);


                    string Apr_Qty = Convert.ToString(dr["Apr_Qty"]);
                    string Apr_Sales_Value = Convert.ToString(dr["Apr_Sales_Value"]);

                    string May_Qty = Convert.ToString(dr["May_Qty"]);
                    string May_Sales_Value = Convert.ToString(dr["May_Sales_Value"]);

                    string Jun_Qty = Convert.ToString(dr["Jun_Qty"]);
                    string Jun_Sales_Value = Convert.ToString(dr["Jun_Sales_Value"]);

                    string Jul_Qty = Convert.ToString(dr["Jul_Qty"]);
                    string Jul_Sales_Value = Convert.ToString(dr["Jul_Sales_Value"]);

                    string Aug_Qty = Convert.ToString(dr["Aug_Qty"]);
                    string Aug_Sales_Value = Convert.ToString(dr["Aug_Sales_Value"]);

                    string Sep_Qty = Convert.ToString(dr["Sep_Qty"]);
                    string Sep_Sales_Value = Convert.ToString(dr["Sep_Sales_Value"]);

                    string Oct_Qty = Convert.ToString(dr["Oct_Qty"]);
                    string Oct_Sales_Value = Convert.ToString(dr["Oct_Sales_Value"]);

                    string Nov_Qty = Convert.ToString(dr["Nov_Qty"]);
                    string Nov_Sales_Value = Convert.ToString(dr["Nov_Sales_Value"]);

                    string Dec_Qty = Convert.ToString(dr["Dec_Qty"]);
                    string Dec_Sales_Value = Convert.ToString(dr["Dec_Sales_Value"]);

                    string Jan_Qty = Convert.ToString(dr["Jan_Qty"]);
                    string Jan_Sales_Value = Convert.ToString(dr["Jan_Sales_Value"]);

                    string Feb_Qty = Convert.ToString(dr["Feb_Qty"]);
                    string Feb_Sales_Value = Convert.ToString(dr["Feb_Sales_Value"]);

                    string Mar_Qty = Convert.ToString(dr["Mar_Qty"]);
                    string Mar_Sales_Value = Convert.ToString(dr["Mar_Sales_Value"]);



                    int Apr_Res = fnInsertDataToDetails("Apr", "4", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, Apr_Qty, Apr_Sales_Value, filename);

                    int May_Res = fnInsertDataToDetails("May", "5", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, May_Qty, May_Sales_Value, filename);

                    int jun_Res = fnInsertDataToDetails("Jun", "6", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, Jun_Qty, Jun_Sales_Value, filename);

                    int Jul_Res = fnInsertDataToDetails("July", "7", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, Jul_Qty, Jul_Sales_Value, filename);

                    int Aug_Res = fnInsertDataToDetails("Aug", "8", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, Aug_Qty, Aug_Sales_Value, filename);

                    int Sep_Res = fnInsertDataToDetails("Sept", "9", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, Sep_Qty, Sep_Sales_Value, filename);

                    int oct_Res = fnInsertDataToDetails("Oct", "10", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, Oct_Qty, Oct_Sales_Value, filename);

                    int Nov_Res = fnInsertDataToDetails("Nov", "11", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, Nov_Qty, Nov_Sales_Value, filename);

                    int Dec_Res = fnInsertDataToDetails("Dec", "12", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, Dec_Qty, Dec_Sales_Value, filename);




                    int jan_Res = fnInsertDataToDetails("Jan", "1", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, Jan_Qty, Jan_Sales_Value, filename);


                    int Feb_Res = fnInsertDataToDetails("Feb", "2", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, Feb_Qty, Feb_Sales_Value, filename);

                    int march_Res = fnInsertDataToDetails("Mar", "3", Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, Mar_Qty, Mar_Sales_Value, filename);


                  lblStatus.Text = "Insert Complete for Month : Count : ---" + inserted_Cnt;
                    Application.DoEvents();

                    //UpdateStatus(a);

                    button1.Visible = true;
                    btnDayWise.Visible = true;
                }

            }
            else {

                MessageBox.Show("Cannot delete record");
            }

        }

        private int fnDeleteAllRecord()
        {
            try
            {
                string qry = "TRUNCATE table [GOAL_SHEET_PART_PLANNING_MONTH]";
                int i = DBUtils.ExecuteSQLCommand(new System.Data.SqlClient.SqlCommand(qry));
                //if (i > 0)
                //{
                    lblStatus.Text= "All Record Deleted";
                Application.DoEvents();
                    //UpdateStatus(a);
                   return 1;
                //}
                //else
                //{
                //    return 0;
                //}
            }
            catch (Exception ex)
            {

                return 0;
            }
        }

        private int fnInsertDataToDetails(string month,string monthNumber,string Sales_Plant,string Category,string Segment,string Cust_Code, string Cust_Group,string Cust_Name,string Type,String Prod_IPD,string Material_Code,string Material_Description,string Cust_Part,string Existing_Rate,string Currency,string Rate_Per,string Rate_Unit,string Financial_Year,string Qty,string Sales,string fname) {

            try
            {

               
                    string qry = "INSERT INTO [GOAL_SHEET_PART_PLANNING_MONTH]([Sales_Plant],[Category],[Segment],[Cust_Code],[Cust_Group],[Cust_Name],[Type],[Prod_IPD],[Material_Code],[Material_Description],[Cust_Part],[Existing_Rate],[Currency],[Rate_Per],[Rate_Unit],[Financial_Year],[MonthNo],[Month_Name],[Qty] ,[Sales]) VALUES ('" + Sales_Plant.Replace("'", "''") + "','"+ Category.Replace("'", "''") + "','"+ Segment.Replace("'", "''") + "','"+ Cust_Code.Replace("'", "''") + "','"+ Cust_Group.Replace("'", "''") + "','"+ Cust_Name.Replace("'", "''") + "','"+ Type.Replace("'", "''") + "','"+ Prod_IPD.Replace("'", "''") + "','"+ Material_Code.Replace("'", "''") + "','"+ Material_Description.Replace("'", "''") + "','"+ Cust_Part.Replace("'", "''") + "',NULLIF('" + Existing_Rate.Replace("'", "''") + "', ''),'" + Currency.Replace("'", "''") + "',NULLIF('" + Rate_Per.Replace("'", "''") + "', ''),'" + Rate_Unit.Replace("'", "''") + "','"+ Financial_Year.Replace("'", "''") + "','"+ monthNumber.Replace("'", "''") + "','"+month.Replace("'", "''") + "',NULLIF('" + Qty.Replace("'", "''") + "', ''),NULLIF('" + Sales.Replace("'", "''") + "', ''))";

                int res = DBUtils.ExecuteSQLCommand(new System.Data.SqlClient.SqlCommand(qry));

                    if(res > 0) {
                   
                    lblStatus.Text= "Inserted at MonthWise: " + inserted_Cnt.ToString();
                    Application.DoEvents();
                    //UpdateStatus(a);

                    inserted_Cnt++;
                    fnCreateLog("Inserted", fname);


                    return 1;

                  

                }
                else {

                    fnCreateLog("Insert Failed for " + month  +  " ; " + monthNumber + " ; " + Segment + " ; " + Segment + " ; " + Cust_Code + " - "  + Cust_Name  +  " ; ", fname);
                    return 0;
                }

                
            }
            catch (Exception ex)
            {
                fnCreateLog("Insert Failed for " + month + " ; " + monthNumber + " ; " + Segment + " ; " + Segment + " ; " + Cust_Code + Cust_Name + " ; " +   "----------" +  ex.ToString(), fname);
                throw ex;
            }


        }



        private void fnCreateLog(string message,string fileName)
        {
            string path = Application.StartupPath + @"\Config\" + fileName ;


            //string path = @"E:\AppServ\Example.txt";

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();

                using (TextWriter tw = new StreamWriter(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                }

            }
            else if (File.Exists(path))
            {

                using (StreamWriter tw = File.AppendText(path))
                {
                    tw.WriteLine("-----------------------------------------------------------------------------");

                    tw.WriteLine("Date : " + DateTime.Now.ToString());
                    tw.WriteLine("");
                    tw.WriteLine("Message : " + message);
                    tw.WriteLine("-----------------------------------------------------------------------------");
                    tw.Close();
                }

               
            }




            //if (System.IO.File.Exists(filePath))
            //{

            //    using (StreamWriter writer = new StreamWriter(filePath, true))
            //    {
            //        writer.WriteLine("-----------------------------------------------------------------------------");
            //        writer.WriteLine("Date : " + DateTime.Now.ToString());
            //        writer.WriteLine();
            //        writer.WriteLine("Message : " + message);

            //        writer.WriteLine("-----------------------------------------------------------------------------");
            //    }
            //}
        }

        private void btnDayWise_Click(object sender, EventArgs e)
        {
            try
            {
                button1.Visible = false;
                btnDayWise.Visible = false;

                int res= fnDeleteDayWiseAllRecord();

                if (res == 1)
                {
                    string qry = "select * from GOAL_SHEET_PART_PLANNING_MONTH ";
                    DataTable details_DayWise = DBUtils.SQLSelect(new System.Data.SqlClient.SqlCommand(qry));

                    string filename = "Day_" + DateTime.Now.ToString("dd-MMM-yyyy HH-MM") + ".txt";
                    foreach (DataRow  dr in details_DayWise.Rows)
                    {


                        string Sales_Plant = Convert.ToString(dr["Sales_Plant"]);
                        string Category = Convert.ToString(dr["Category"]);
                        string Segment = Convert.ToString(dr["Segment"]);
                        string Cust_Code = Convert.ToString(dr["Cust_Code"]);
                        string Cust_Group = Convert.ToString(dr["Cust_Group"]);
                        string Cust_Name = Convert.ToString(dr["Cust_Name"]);
                        string Type = Convert.ToString(dr["Type"]);
                        string Prod_IPD = Convert.ToString(dr["Prod_IPD"]);
                        string Material_Code = Convert.ToString(dr["Material_Code"]);
                        string Material_Description = Convert.ToString(dr["Material_Description"]);
                        string Cust_Part = Convert.ToString(dr["Cust_Part"]);
                        string Existing_Rate = Convert.ToString(dr["Existing_Rate"]);
                        string Currency = Convert.ToString(dr["Currency"]);
                        string Rate_Per = Convert.ToString(dr["Rate_Per"]);
                        string Rate_Unit = Convert.ToString(dr["Rate_Unit"]);
                        string Financial_Year = Convert.ToString(dr["Financial_Year"]);

                        string MonthNo = Convert.ToString(dr["MonthNo"]);
                        string Month_Name = Convert.ToString(dr["Month_Name"]);
                        string Qty = Convert.ToString(dr["Qty"]);
                        string Sales = Convert.ToString(dr["Sales"]);


                        string MonthDays = fnGetNumOfDaysInMonth(MonthNo);

                        double DayQty = Convert.ToDouble(Qty) / Convert.ToDouble(MonthDays);
                        double DaySales = Convert.ToDouble(Sales) / Convert.ToDouble(MonthDays);


                        for (int i = 1; i <= Convert.ToInt32(MonthDays); i++) {

                            double ProgressQty = i * DayQty;
                            double ProgressSales = i * DaySales;
                            

                            string runningDate = fnGetRunningDate(i, MonthNo);

                            fnInsertDataTo_PlanningData(Sales_Plant, Category, Segment, Cust_Code, Cust_Group, Cust_Name, Type, Prod_IPD, Material_Code, Material_Description, Cust_Part, Existing_Rate, Currency, Rate_Per, Rate_Unit, Financial_Year, MonthNo, Month_Name, Qty, Sales, MonthDays, runningDate, Convert.ToString(DayQty),Convert.ToString( DaySales), Convert.ToString( ProgressQty),Convert.ToString( ProgressSales), filename);

                        }


                    }

                     lblStatus.Text = "Insert Completed For Day Wise plan : Count : ---" + insert_Cnt_day;

                    button1.Visible = true;
                    btnDayWise.Visible = true;

                }
                else {
                    MessageBox.Show("Failed to delete record...");

                }




           }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw ex;
            }
        }

        private string fnGetRunningDate(int date, string monthNo)
        {
            string qry = "  select [DateOfYear] from [GOAL_SHEET_Dates] where  month([DateOfYear])='"+ monthNo + "' and day([DateOfYear]) = '"+ Convert.ToString(date) + "'";

            string dateValue = DBUtils.SqlSelectScalar(new System.Data.SqlClient.SqlCommand(qry));
            return dateValue;
        }

        private void fnInsertDataTo_PlanningData(string Sales_Plant,string Category,string Segment,string Cust_Code,string Cust_Group,string Cust_Name,string Type,string Prod_IPD,string Material_Code,string Material_Description,string Cust_Part,string Existing_Rate,string Currency,string Rate_Per,string Rate_Unit,string Financial_Year,string MonthNo,string Month_Name,string Qty,string Sales, string MonthDays,string Date,string DayQty,string DaySales,string ProgressQty,string ProgressSales,string fname)
        {
            try
            {


               

                string qry = "INSERT INTO [GOAL_SHEET_PART_PLANNING_DAY] ([Sales_Plant],[Category],[Segment],[Cust_Code],[Cust_Group],[Cust_Name],[Type],[Prod_IPD],[Material_Code],[Material_Description],[Cust_Part],[Existing_Rate],[Currency],[Rate_Per],[Rate_Unit],[Financial_Year],[MonthNo],[Month_Name],[Qty],[Sales],[MonthDays],[Date],[DayQty],[DaySales],[ProgressQty],[ProgressSales])     VALUES('"+Sales_Plant.Replace("'", "''") + "','" + Category.Replace("'", "''") + "','" + Segment.Replace("'", "''") + "','"+ Cust_Code.Replace("'", "''") + "','"+Cust_Group.Replace("'", "''") + "','"+Cust_Name.Replace("'", "''") + "','"+Type.Replace("'", "''") + "','"+ Prod_IPD.Replace("'", "''") + "','"+ Material_Code.Replace("'", "''") + "','"+Material_Description.Replace("'", "''") + "','"+Cust_Part.Replace("'", "''") + "',NULLIF('" + Existing_Rate.Replace("'", "''") + "', ''),'" + Currency.Replace("'", "''") + "',NULLIF('" + Rate_Per.Replace("'", "''") + "', ''),NULLIF('" + Rate_Unit.Replace("'", "''") + "', ''),'" + Financial_Year+"','"+MonthNo+"','"+Month_Name+"','"+Qty+"','"+Sales+"','"+MonthDays+"','"+Date+"','"+DayQty+"','"+DaySales+"','"+ProgressQty+"','"+ProgressSales+"')";

                int res = DBUtils.ExecuteSQLCommand(new System.Data.SqlClient.SqlCommand(qry));

                if (res > 0)
                {
                    lblStatus.Text = "Inserted At daywise,Count : " + insert_Cnt_day;
                    insert_Cnt_day++;
                    fnCreateLog("Inserted", fname);
                    Application.DoEvents();
                }
                else {

                    fnCreateLog("Insert failed", fname);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private string fnGetNumOfDaysInMonth(string monthNo)
        {
            try
            {
                string qry="select Distinct [DaysOfMonth] from[GOAL_SHEET_Dates] where month([DateOfYear]) = '"+ monthNo + "' ";

                string res = DBUtils.SqlSelectScalar(new System.Data.SqlClient.SqlCommand(qry));
                return res;

            }
            catch (Exception ex)
            {

                return "0";
            }

        }

        private int fnDeleteDayWiseAllRecord()
        {
            try
            {
                string qry = "TRUNCATE table [GOAL_SHEET_PART_PLANNING_DAY]";
                int i = DBUtils.ExecuteSQLCommand(new System.Data.SqlClient.SqlCommand(qry));
              
                   lblStatus.Text= "All Record Deleted";
                Application.DoEvents();
                    //UpdateStatus(a);
                   return 1;
                //}
                //else
                //{
                //    return 0;
                //}
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
    }


 
   
}
